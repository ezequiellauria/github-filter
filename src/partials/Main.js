import React from 'react';
import { Switch, Route } from 'react-router-dom';

/* --- REACT VIEWS --- */

import Home from '../views/Home';
import FilteredUser from '../views/FilteredUser';
import RepoActivity from '../views/RepoActivity';

const Main = () => (
    <section id="main-content" role="main">
        <div className="container">
            <main>
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route path='/user/:user' component={FilteredUser} />
                    <Route path='/contributors/:user/:repo' component={RepoActivity}/>
                    <Route component={Home}/>
                </Switch>
            </main>
        </div>
    </section>
)

export default Main
