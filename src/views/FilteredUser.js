import React from 'react';

import RepoListContainer from '../components/RepoList';

import FilteredUserCSS from '../css/FilteredUser.css';

class FilteredUser extends React.Component {

    constructor(props) {
        super();

        this.props = props;
        this.fetchTimeout = 0;

        this.state = {
            user : null
        }

    }

    componentDidMount() {
        this.loadUser();
    }

    componentWillReceiveProps(nextProps) {
        if ( this.props.match.params.user !== nextProps.match.params.user ) {
            this.props = nextProps;
            this.loadUser();
        }
    }

    showUser(user) {
        if ( !Array.isArray(user) || user.length === 0 ) {
            this.props.history.push('/');
        } else {
            this.setState({
                user: user
            });
        }
    }

    loadUser() {
        clearTimeout(this.fetchTimeout);
        var self = this;


        this.fetchTimeout = setTimeout( function() {
            fetch(`https://api.github.com/users/${self.props.match.params.user}/repos`).then(response => response.json())
                .then(response => self.showUser(response));
        }, 500 );

    }

    render() {
        if (this.state.user) {
            return (
                <div>
                    <h1>{this.props.match.params.user}'s repositories</h1>
                    <RepoListContainer repos={this.state.user} />
                </div>
            );
        }

        return null;
    }
}

export default FilteredUser
