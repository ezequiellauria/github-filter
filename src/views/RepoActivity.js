import React from 'react';

import {Line} from 'react-chartjs-2';

class RepoActivity extends React.Component {

    constructor(props) {
        super();

        this.props = props;
        this.fetchTimeout = 0;

        this.state = {
            repo : null
        }

    }

    componentDidMount() {
        this.loadRepo();
    }

    componentWillReceiveProps(nextProps) {
        if (
            ( this.props.match.params.user !== nextProps.match.params.user ) ||
            ( this.props.match.params.repo !== nextProps.match.params.repo )
        ) {
            this.props = nextProps;
            this.loadRepo();
        }
    }

    showRepo(repo) {
        if ( !Array.isArray(repo) || repo.length === 0 ) {
            this.props.history.push('/');
        } else {
            this.setState({
                repo: repo
            });
        }
    }

    loadRepo() {
        if (
            !this.props.match.params.user ||
            !this.props.match.params.repo
        ) {
            this.props.history.push('/');
        }

        clearTimeout(this.fetchTimeout);
        var self = this;

        let params = this.props.match.params;

        this.fetchTimeout = setTimeout( function() {
            fetch(`https://api.github.com/repos/${params.user}/${params.repo}/contributors`).then(response => response.json())
                .then(response => self.showRepo(response));
        }, 500 );

    }

    render() {
        if (this.state.repo) {
            var labels = [];
            var dataList = [];

            this.state.repo.forEach((apiData)=>{
               labels.push(apiData.login);
                dataList.push(apiData.contributions);
            });

            var tempData = {
                labels: labels,
                datasets: [{
                    data: dataList
                }]
            };

            var options = {
                legend: {
                    display: false
                }
            };

            return (
                <div>
                    <h1>{this.props.match.params.repo}'s activity</h1>
                    <Line options={options} data={tempData} />
                </div>
            );
        }

        return null;
    }
}

export default RepoActivity
