import React from 'react';

import UserListContainer from '../components/UserList';

import HomeCSS from '../css/Home.css';

const GITHUB_API = 'https://api.github.com/';

class HomePage extends React.Component {

    constructor() {
        super();

        this.keyTimeout = 0;

        this.state = {
            users : []
        }
    }

    showUsers(users) {
        this.setState({
            users: users.items
        })
    }

    searchUser(e) {
        let value = e.target.value,
            self = this;
        clearTimeout(this.keyTimeout);
        this.keyTimeout = setTimeout( function() {
            fetch(`${GITHUB_API}search/users?q=${value}`).then( response => response.json() )
                .then( response => self.showUsers(response) );
        }, 500 );
    }

    render() {
        return (
            <div>
                <Widget searchUser={this.searchUser.bind(this)} />
                <UserListContainer users={this.state.users} />
            </div>
        );
    }
}

const Widget = (props) =>
    <input className="user-search" placeholder="Please write a Github username" type="text" onChange={props.searchUser} />

export default HomePage
