import React, { Component } from 'react';

import Header from '../partials/Header';
import Main from '../partials/Main';

class MainLayout extends Component {
    render() {
        return (
            <section>
                <Header />
                <Main />
            </section>
        );
    }
}
export default MainLayout;
