import React from 'react';
import { Link } from 'react-router-dom'

class UserListContainer extends React.Component {
    render() {
        let userList = this.props.users;

        if (userList.length === 0) return null;

        return (
            <ul className="user-list">
                {userList.map(this.createListItem)}
            </ul>
        );
    }

    createListItem(user) {
        return (
            <li className="user-list__item" key={user.id}>
                <Link to={`/user/${user.login}`}>
                    <img alt={user.login} src={user.avatar_url} /> {user.login}
                </Link>
            </li>
        );
    }
}

export default UserListContainer
