import React from 'react';
import { Link } from 'react-router-dom'

class RepoListContainer extends React.Component {
    render() {
        let repoList = this.props.repos;

        if (repoList.length === 0) return null;

        return (
            <ul className="repo-list">
                {repoList.map(this.createListItem)}
            </ul>
        );
    }

    createListItem(repo) {
        return (
            <li key={repo.id}>
                <Link to={`/contributors/${repo.full_name}`}>
                    {repo.name}
                </Link>
            </li>
        );
    }
}

export default RepoListContainer
