import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter } from 'react-router-dom';

import styles from './index.css';

/* --- LAYOUTS --- */

import MainLayout from './layouts/MainLayout';

ReactDOM.render(
    <HashRouter>
        <MainLayout />
    </HashRouter>
,document.getElementById('root'));
